package com.binus;

public class Main {

    public static void printTriangle(int n) {
        int i, j;

        // outer loop to handle number of rows
        //  n in this case
        for(i=1; i<n; i++)
        {

            //  inner loop to handle number of columns
            //  values changing acc. to outer loop
            for(j=10; j>i; j--)
            {
                // printing stars
                System.out.print("* ");
            }

            // ending line after each row
            System.out.println();
        }
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");
    int n=10;

    printTriangle(n);

    }


}
