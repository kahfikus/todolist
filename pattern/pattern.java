package pattern;

public class pattern {
    public static void printTriangle(int n)
    {
        int i, j;


        for(i=0; i<n; i++)
        {


            for(j=0; j<=i; j++)
            {

                System.out.print("* ");
            }


            System.out.println();
        }
    }

    public static void main(String[] args) {

        int n=10;
        printTriangle(n);
    }
}
